#SingleInstance force ; Make sure only one instance of the script is running at a time
#NoENV ; Avoid checking empty variables to see if they are environment variables (recommended for all new scripts and increases performance).
#include mysql.ahk ; MySQL Library
#include DB_Functions.ahk ; Database functions
#include IE_Functions.ahk ; Internet Explorer functions
#include INI_Functions.ahk ; ini file functions
SetTitleMatchMode 2 ; A window's title can contain WinTitle anywhere inside it to be a match. 
; ListLines On ; a debugging option




; AUTORUN
Gosub, StartUp ; Initialize the application
^+x::Gosub, ButtonPost ; Assign Hotkey Ctrl + Shft +  x
^+c::Gosub, ButtonPause ; Assign Hotkey Ctrl + Shft + c
^+z::Gosub, ButtonStop ; Assign Hotkey Ctrl + Shft + z
^+q::Gosub, MenuOptionsExit ; Assign Hotkey Ctrl + Shft + q
return ; End of Autorun




StartUp: ; RUNS AT APP START UP
	LogMsg("Starting Appliction")
	Global SuccessCount := 0 ; Integer, the number of successful posts
	Global FailCount := 0 ; Integer, the number of unsuccessful posts. This number may be lower the the actual number of App stallouts
	Gosub, GetAppData ; Gets Settings and app option data
	Gosub, GUI ; Display the App
	Gosub, SetAppSettings ; Set App controls according to ini file
	open_Wampserver() ; Open WampSever if not running already
	open_TeamLoser() ; Open TeamLoser if not running already
	SetStatusBar("Ready") ; Update app status bar
return ; END StartUp




Post: ; Our main Appliction loop
	Global ieMain ; Make Internet Explorer object available
	if checkForStop() ; Check if a Stop is queued or if the posting limit has been reached, if not run the loop
	{
		; ResetPost
		if ( LoopStarted = 1 AND LoopComplete = 0 )
			Gosub, ResetPost ; 
		LoopStarted = 1 ; 
		LoopComplete = 0 ; 
		Global SuccessCount ; Make vars available

		; Check Posts
		if ( App_Mode = "Make Gmail" ) { ; If 'Create Accounts' is the selected App 'Mode'
			get_new_account()
			open_IE() ; Close any IE windows that are open, create a new Internet Explorer shell object and store it in a global var
			make_gmail_account() ; Make a new email
		}

		; Check Posts
		if ( App_Mode = "Check Posts" ) { ; If 'Create Accounts' is the selected App 'Mode'
			get_post()
			open_IE() ; Close any IE windows that are open, create a new Internet Explorer shell object and store it in a global var
			check_post_status()
		}

		; Post & Verify
		if ( App_Mode = "Post & Verify" ) { ; If 'Create Accounts' is the selected App 'Mode'
			get_ad() ; Get an ad from the database & store it in global vars
			download_attachments()
			get_account() ; Fetch cl account from DB and store in global vars
			get_campaign()
			get_template()
			open_IE() ; Close any IE windows that are open, create a new Internet Explorer shell object and store it in a global var
			login_craigslist() ; Go to Craigslist and log in
			post_ad() ; Post the ad
			can_verify_account() ; Check if we can phone verify, get a publish link or have to post again
		}

	;	get_new_account()
	;	make_gmail_account() ; Make a new email
	
	;	; Create Account
	;	if ( App_Mode = "Create CL Accounts" ) { ; If 'Create Accounts' is the selected App 'Mode'
	;		get_new_account() ; Get new account details
	;;		create_account() ; Create cl account, store in global vars
	;	}
	;	; Post & Renew
	;	if ( App_Mode = "Post & Renew" ) {
	;		
	;	}
	;	; Create Accounts
	;	if ( App_Mode = "Create Accounts" ) { ; If 'Create Accounts' is the selected App 'Mode'
	;	}
		; Update Status Bar

		LoopComplete = 1 ; 
		SuccessCount++
		; Reset the IP Address
		GuiControl, Disable, Button2 ; Disable 'Pause' button
		GuiControl, Disable, Button3 ; Disable 'Stop' button
		WinClose, ahk_class IEFrame ; Close Internet Explorer
		Gosub, ResetIP ; Reset the IP Address
		GuiControl, Enable, Button2 ; Enable 'Pause' button
		GuiControl, Enable, Button3 ; Enable 'Stop' button
		Post() ; Make another post
	}
return ; END Post

Post() {
	Global App_Mode ; Make vars available
	if ( App_Mode = "Create Accounts" ) ; If 'Create Accounts' is the selected App 'Mode'
		SetTimer, Post, 1005000 ; & set it on a timer, ~ 17 mins ( mins*secs*milisecs ) - IMPORTANT, This must be set long enough for the posting process & IP reset to complete
	else if ( App_Mode = "Post & Renew" ) ; If 'Post & Renew"' is the selected App 'Mode'
		SetTimer, Post, 350000 ; & set it on a timer, 5 mins ( mins*secs*milisecs ) - IMPORTANT, This must be set long enough for the posting process & IP reset to complete
	else ; If 'Post Only"' is the selected App 'Mode'
		;SetTimer, Post, 250000 ; & set it on a timer, ~ 3.5 mins ( mins*secs*milisecs ) - IMPORTANT, This must be set long enough for the posting process & IP reset to complete
		SetTimer, Post, 1005000 ; & set it on a timer, ~ 17 mins ( mins*secs*milisecs ) - IMPORTANT, This must be set long enough for the posting process & IP reset to complete
	Gosub, Post ; Run the Posting Process right away, since the timer won't start right away
} ; END Post

checkForStop() { ; Checks if a 'Stop' is queued or if the posting limit has been reached. If so, Prevents posting & sets GUI control states accordingly
	Global Stop, App_PostLimit ; Make vars available
	if ( Stop = 1 ) { ; If a stop is queued, stop the Process
		WinMove, Post Juice,, posX, posY,, 520,
		GuiControl, Enable, Button1 ; Enable 'Post' button
		GuiControl, Disable, Button2 ; Disable 'Pause' button
		GuiControl, Disable, Button3 ; Disable 'Stop' button
		Stop = 0 ; Reset 'Stop' (false)
		LoopStarted = 0
		SetTimer, Post, Off ; Stop posting
		SetStatusBar("Ready") ; Update app status bar
	}
	else if ( App_PostLimit = 0 ) ; If the Post Limit is set to 0(unlimited), return true
		return true
	else if ( A_Index < App_PostLimit ) ; If the number if post made is less than the Post Limit, return true
		return true
	else if ( A_Index = App_PostLimit ) { ; If the number if post made equals the Post Limit, queue a stop & return true
		Stop = 1
		return true
	}
} ; END checkForStop()




GUI: ; Builds the GUI
	Global client_options ; Make vars available
	; ADD MENU
	Menu, OptionsMenu, Add, Show/Hide Advanced Options, MenuOptionsAdvancedOps
	Menu, OptionsMenu, Add, Save Settings, MenuOptionsSave
	Menu, OptionsMenu, Add, Reset Post Process, MenuOptionsResetProcess
	Menu, OptionsMenu, Add, Reset IP Address, MenuOptionsResetIP
	Menu, OptionsMenu, Add, Exit, MenuOptionsExit
	Menu, HelpMenu, Add, App Manual, MenuHelpAbout
	Menu, AppMenuBar, Add, Options, :OptionsMenu  ; Attach the two sub-menus that were created above.
	Menu, AppMenuBar, Add, Help, :HelpMenu
	Gui, Menu, AppMenuBar
	; ADD ACTION BUTTONS
	Gui, Font, norm bold s20, Lobster ; Set the font
	Gui, Add, Button, x150 y-0 w200 h50, Post ; Add 'Post' button
	Gui, Font, norm bold s16, Lobster ; Set the font
	Gui, Add, Button, x350 y-0 w150 h50, Pause ; Add 'Pause' button
	Gui, Add, Button, x-0 y-0 w150 h50, Stop ; Add 'Stop' button
	; ADD TABBEB AREA
	Gui,Font, norm cF0F5F5 s10, MS Reference Sans Serif ; Set the font
	Gui, Add, Tab2, vAppTabs x0 y50 w500 h450, Basic|Help|Advanced ; Tabbed area dims & 'Tabs'
	Gui,Font, norm cF0F5F5 s12, MS Reference Sans Serif ; Set the font
		; TAB 'Basic'
		Gui, Add, Text, x25 y105 w150 h50, Mode: ; Label Mode
		Gui, Add, DropDownList, x200 y100 w275 h100 vApp_Mode, Make Gmail|Check Posts|Create Accounts|Post & Verify||Post Only|Renew only ; Double 'pipe' after default option
		Gui, Add, Text, x25 y155 w150 h50, Client: ; Label Client
		Gui, Add, DropDownList, x200 y150 w275 h100 vApp_Client, client_options ; Double 'pipe' after default option
		Gui, Add, Text, x25 y205 w150 h50, Post Limit: ; Label Post Limit
		Gui, Add, Edit, x200 y200 w275 h30 vApp_PostLimit,
		Gui,Font, norm cF0F5F5 s10, MS Reference Sans Serif ; Set the font
		Gui, Add, Text, x200 y238 w275 h100, Enter the number of posts you would like to make, or enter 0 to post the entire queue ; Description Post Limit
		Gui,Font, norm cF0F5F5 s12, MS Reference Sans Serif ; Set the font
		Gui, Add, Text, x25 y305 w150 h50, Autosave: ; Label Autosave
		Gui, Add, Checkbox, x200 y300 w25 h30 vApp_AutoSave gAutosave,
		Gui,Font, norm cF0F5F5 s10, MS Reference Sans Serif ; Set the font
		Gui, Add, Text, x225 y305 w250 h90, Check this box if you want to autosave your selected settings on program exit. ; Description Autosave
		Gui,Font, norm cF0F5F5 s12, MS Reference Sans Serif ; Set the font
	Gui, Tab, 2
		; TAB 'Help'
		Gui, Add, Text, x25 y105 w150 h50, Help! ; Label
	Gui, Tab, 3
		; TAB 'Advanced'
		Gui, Add, Text, x25 y105 w150 h50, MySQL Hostname: ; Label Database Name
		Gui, Add, Edit, x200 y100 w275 h30 vMysql_Host,
		Gui, Add, Text, x25 y155 w150 h50, Database Name: ; Label Database Name
		Gui, Add, Edit, x200 y150 w275 h30 vMysql_Database,
		Gui, Add, Text, x25 y205 w150 h50, Username: ; Label Username
		Gui, Add, Edit, x200 y200 w275 h30 vMysql_Username,
		Gui, Add, Text, x25 y255 w150 h50, Password: ; Label Password
		Gui, Add, Edit, x200 y250 w275 h30 vMysql_Password,
		Gui, Add, Text, x25 y305 w150 h50, Twilio SID: ; Label Twilio SID
		Gui, Add, Edit, x200 y300 w275 h30 vTwilio_SID,
		Gui, Add, Text, x25 y355 w150 h50, Auth Token: ; Label Auth Token
		Gui, Add, Edit, x200 y350 w275 h30 vTwilio_AuthToken,
		Gui, Add, Text, x25 y405 w150 h50, DBC Username: ; Label Death By Cptcha Username
		Gui, Add, Edit, x200 y400 w275 h30 vDBC_Username,
		Gui, Add, Text, x25 y455 w150 h50, Password: ; Label Death By Cptcha Password
		Gui, Add, Edit, x200 y450 w275 h30 vDBC_Password,
	Gui, Tab  ; subsequently-added controls will not belong to the tab control.
	; SET BUTTON STATES
	GuiControl, Disable, Button2 ; Disable 'Pause' button
	GuiControl, Disable, Button3 ; Disable 'Stop' button
	; APPLICATION WINDOW
	Gui, Font, norm s12,Lobster ; Set the font
	Gui, Add, StatusBar,, Starting up... ; Add a stus bar to the GUI
	Gui, Color, 141F1F, 1F2E2E ; The window color - WindowColor, ControlColor
	Gui, +AlwaysOnTop ; Always show the App on top of the active window
	Gui, Show, w500 h520 Center, Post Juice ; The GUI window
return ; END GUI




MenuOptionsAdvancedOps: ; Menu Callback - Toggles displaying the 'Advanced Settings' tab
	Global App_ShowAdOps ; Make vars available
	; TOGGLE
	if ( App_ShowAdOps = 1 ) { ; If True
		App_ShowAdOps = 0 ; Set False
		GuiControl,, AppTabs, |Basic||Help ; Hide tab, Double 'pipe' after default tab option
	}
	else { ; Else False
		App_ShowAdOps = 1 ; Set True
		GuiControl,, AppTabs, |Basic|Help|Advanced|| ; Show tab, Double 'pipe' after default tab option
	}
return

MenuOptionsSave: ; Menu Callback - Saves appliction settings
	Gosub, SaveAppData ; Save App data
return

MenuOptionsResetProcess: ; Menu Callback - Resets the IP Address
	SetStatusBar("Resetting Posting Process") ; Update app status bar
	GuiControl, Disable, Button1 ; Disable 'Post' button
	GuiControl, Enable, Button2 ; Enable 'Pause' button
	GuiControl, Enable, Button3 ; Enable 'Stop' button
	Post() ; Make another post
return

MenuOptionsResetIP: ; Menu Callback - Resets the IP Address
	GuiControl, Disable, Button1 ; Disable 'Post' button
	GuiControl, Disable, Button2 ; Disable 'Pause' button
	GuiControl, Disable, Button3 ; Disable 'Stop' button
	Gosub, ResetIP ; Reset the IP Address
	GuiControl, Enable, Button1 ; Enable 'Post' button
	SetStatusBar("Ready") ; Update app status bar
return

MenuOptionsExit: ; Menu Callback - Exits the Appliction
	Gosub, AutoSave ; Save App Settings if option is enabled
	IfWinExist, ahk_class IEFrame ; If IE has windows open
	{
		GroupAdd, IEWindows , ahk_class IEFrame ; Add them to a group,
		GroupClose, IEWindows, A ; and close the group.
	}
	ExitApp ; Close the Application
return

MenuHelpAbout: ; Menu Callback - Opens The 'Help' tab
	GuiControl, Choose, AppTabs, 4 ; The 'Help' Tab is the 4th value in the tabc control index
return




ButtonPost: ; Runs when the 'Post' button is clicked
	GuiControl, Disable, Button1 ; Disable 'Post' button
	GuiControl, Enable, Button2 ; Enable 'Pause' button
	GuiControl, Enable, Button3 ; Enable 'Stop' button
	SetStatusBar("Posting") ; Update app status bar
	WinGetPos, posX, posY,,, Post Juice,
	WinMove, Post Juice,, A_ScreenWidth - 510, A_ScreenHeight - 125,, 120,
	Gui, Submit, NoHide ; Get App control values
	Post() ; Start Posting Process
return ; END ButtonPost

ButtonPause: ; Runs when the "Pause" button is clicked	
	if ( A_IsPaused = 0 ) { ; If not 'Paused'
		WinMove, Post Juice,, posX, posY,, 520,
		StatusBarGetText, beforeStatus,, Post Juice ; Get the current status bar text
		GetStatusBar()
		GuiControl, Disable, Button3 ; Disable 'Stop' button
		SetStatusBar("Paused") ; Update app status bar
	}
	else { ; Else is 'Paused'
		WinMove, Post Juice,, A_ScreenWidth - 510, A_ScreenHeight - 125,, 120,
		GuiControl, Enable, Button3 ; Enable 'Stop' button
		SetStatusBar(BeforeStatus) ; Update app status bar
	}
	Pause, Toggle, 1 ; Pause/Unpause the Thread
return ; END : ButtonPause

ButtonStop: ; Runs when the "Stop" button is clicked
	Global Stop ; Make vars available
	Stop = 1 ; Set 'Stop' true
	GuiControl, Disable, Button3 ; Disable 'Stop' button
return ; END ButtonStop




SetStatusBar( statusText ) {
	Global Stop, SuccessCount, FailCount ; Make vars available
	if ( Stop = 1 ) ; If 'Stop' is true
		stop_text = (stop queued)
	SB_SetText( statusText "... " SuccessCount " Complete, " FailCount " Incomplete " stop_text ) ; Update app status bar
} ; END SetStatusBar

GetStatusBar() {
	Global BeforeStatus
	StatusBarGetText, beforeText,, Post Juice ; Get the current status bar text
	RegExMatch( beforeText, "(.+?)\.\.\.", beforeText )
	StringReplace, BeforeStatus, beforeText, `.`.`.,,  ; Remove trailing '...'
} ; END SetStatusBar




GetAppData: ; Gets App data & pref's
	inifile = postjuice.ini ; Set path to ini file local to working directory
	INI_Init(inifile) ; Initiate with ini include
	INI_Load(inifile) ; Load App data
	connect_to_db() ; Makes a connection to the database and stores it in a global var
	get_app_options() ; Gets Options from the database for select controls
return ; END : GetAppData

SaveAppData: ; Saves App current settings as starup pref's
	Gui, Submit, NoHide ; Get all App control values
	if App_Client = ; If no value is selected,
		App_Client = 0 ; Set as 0.
	INI_Save(inifile) ;  Update ini file
return ; END : SaveAppSettings

AutoSave: ; Checks if the 'Autosave Settings' options is checked & saves if true
	Gui, Submit, NoHide ; Get all App control values
	if ( App_AutoSave = 1 ) { ; If True
		Gosub, SaveAppData ; Save All App data
	}
	else { ; Else
		IniWrite, 0, %inifile%, App, AutoSave ; Only save the Autosave option
	}
return ; END AutoSave

SetAppSettings: ; Sets App according to saved ini file settings
	; Advanced Tab
	if ( App_ShowAdOps != 1 ) { ; If Show Advanced Options is not set to true
		GuiControl,, AppTabs, |Basic||Queue|Templates|Help ; Hide 'Advanced' Tab - Double 'pipe' after default option
	}
	; Mode
	modeOpsDefault = Make Gmail|Check Posts|Create & Verify|Post & Verify|Post Only|Renew only ; Build Mode Options string with a double pipe after the selected option 
	modeOps = | ; Make a new option string and start it with a pipe since we're replacing the old options
	Loop, Parse, modeOpsDefault, | ; Loop thru the default options
	{
		modeOps = %modeOps%%A_LoopField%| ; Add the option to our new option string
		if ( App_Mode = A_LoopField ) { ; If the feid was selected
			modeOps = %modeOps%| ; Add an extra pipe to make that option selected
		}
	}
	GuiControl,, ComboBox1, % modeOps ; Set 'Basic' tab 'Mode' field
	; Client
	clientOps = | ; Make a new option string abd start it with a pipe since were Replacing the old options
	Loop, Parse, client_options, | ; Loop thru the default options
	{
		clientOps = %clientOps%%A_LoopField%| ; Add the option to our new option string
		if ( App_Client = A_LoopField ) { ; If the feid was selected
			clientOps = %clientOps%| ; Add an extra pipe to make that option selected
		}
	}
	GuiControl,, ComboBox2, % clientOps ; Set 'Basic' tab 'Client' field
	; Post limit
	GuiControl,, Edit1, % App_PostLimit ; Set 'Basic' tab 'Post Limit' field
	; Autosave
	if ( App_AutoSave = 1 ) {
		GuiControl,, Button4, 1 ; Set 'Basic' tab 'Autosave Settings' field
	}
	; Database Name
	GuiControl,, Edit2, % Mysql_Host ; Set 'Advanced' tab 'Database Name' field
	; Database username
	GuiControl,, Edit3, % Mysql_Database ; Set 'Advanced' tab 'Database Name' field
	; Database username
	GuiControl,, Edit4, % Mysql_Username ; Set 'Advanced' tab 'Username' field
	; Database password
	GuiControl,, Edit5, % Mysql_Password ; Set 'Advanced' tab 'Password' field
	; TWilio account SID
	GuiControl,, Edit6, % Twilio_SID ; Set 'Advanced' tab 'Twilio Account SID' field
	; Twilio auth token
	GuiControl,, Edit7, % Twilio_AuthToken ; Set 'Advanced' tab 'Twilio Auth Token' field
	; Death By Captcha username
	GuiControl,, Edit8, % DBC_Username ; Set 'Advanced' tab 'DBC Username' field
	; Death By Captcha password
	GuiControl,, Edit9, % DBC_Password ; Set 'Advanced' tab 'DBC Password' field
return ; END SetAppSettings




ResetPost: ; Restarts the Posting Process
	Global FailCount, BeforeStatus, LoopStarted ; Make vars available
	FailCount++
	LoopStarted = 0
	LogMsg("Appliction Reset #"%FailCount%)
	SetStatusBar("Resetting Posting Process") ; Update app status bar
	Gosub, ResetIP
	SetStatusBar("Posting") ; Update app status bar
	Post() ; Make another post
return ; END SetStatusBar

ResetIP: ; Resets the IP Address and sleeps while the modem reboots
	Global FailCount, BeforeStatus ; Make vars available
	GetStatusBar() ; Get app status bar text
	SetStatusBar("Resetting IP Address") ; Update app status bar
	Run, "ResetIP.bat" ; Run batch file to reset the IP Address
	Sleep, 90000 ; Sleep until the modem reconnects
	SetStatusBar(BeforeStatus) ; Update app status bar
return ; END ResetIP

LogMsg( message ) { ; appends a message to a log file
	StatusBarGetText, SBText,, Post Juice
	message := message " - (" A_Now ") " SBText "`n"
	FileAppend, %message%, PostJuiceLog.txt
} ; END LogError