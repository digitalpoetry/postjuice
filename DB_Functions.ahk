shiftsRegex( haystack ) {
	haystack2 := RegExReplace(haystack, "([A-Z]|[\!]|[\@]|[\#]|[\$]|[\%]|[\^]|[\&]|[\*]|[\(]|[\)]|[\_]|[\+]|[\<]|[\>]|[\?]|[\{]|[\}]|[\|]|[\~]|[\:]|[""]+?)", "{SHIFTDOWN}$1{SHIFTUP}")
	StringReplace, haystack3, haystack2, {SHIFTUP}{SHIFTDOWN}, , All
	return %haystack3%
}

htmlentitiesEncode( InputStr ) {
	StringReplace, OutputStr, InputStr, &, &amp;, All
	return OutputStr
}

connect_to_db() {
	Global Mysql_Host, Mysql_Database, Mysql_Username, Mysql_Password ; Make vars available
	mysql := new mysql
	db := mysql.connect( Mysql_Host, Mysql_Username, Mysql_Password, Mysql_Database ) ; host, user, password, database
	if db =
	    MsgBox, 262160, Post Juice Error, Connection to the database failed. Check your MySQL credentials and check that the database is running, 10
}




get_app_options() { ; Retrieves initial GUI control options needed during startup
	Global client_options ; New Globar var
	Global clients_array := Object() ;  New global var
	; GET CLIENT OPTIONS
	user_IDs_sql = ( SELECT DISTINCT ad_user_id FROM pj_web_apper_ads ) ; Query the DB for distinct user ID's
	user_IDs := mysql.query(db, user_IDs_sql, 0) ; Get query results 
	if user_IDs =
		MsgBox, 262160, Post Juice Error, No users were found in the system., 10
	Loop, Parse, user_IDs, `n ; Loop thru the returned ID's
	{
		; Query the DB and get the username for each ID
		username_sql = ( SELECT user_login FROM pj_users WHERE ID = '%A_LoopField%' ) ; Prepare MySQL statement
		username := mysql.query(db, username_sql, 0) ; Get query results
		client_options .= username "|" ; Add the username to the options string
		clients_array[username] := A_LoopField ; Store username ID by username in array
	}
	client_options := SubStr( client_options, 1, -1 ) ; Remove the trailing pipe from the options string
}




get_ad() {
	Global App_Client, clients_array ; Make vars available
	For username, userid in clients_array
	{
		if ( App_Client = username )
		{
			ad_sql = ( SELECT ID,ad_user_id,ad_claccount_id,ad_campaign_id,ad_template_id,ad_title,ad_description,ad_zip,ad_use_phone_fields,ad_phone_replies,ad_phone_number,ad_email_replies,ad_contact_name,ad_specific_location,ad_ok_to_contact,ad_show_on_maps,ad_street,ad_cross_street,ad_city,ad_state,ad_img_ids,ad_last_used,ad_created_on,ad_edited_on,ad_queued,ad_price,ad_rent,ad_bedrooms,ad_bathrooms,ad_sqft,ad_housing_type,ad_laundry,ad_parking,ad_furnished,ad_dogs_ok,ad_cats_ok,ad_mls_number FROM pj_web_apper_ads WHERE ad_queued = "1" AND ad_user_id = %userid% ORDER BY ad_last_used ASC LIMIT 1 ) ; get a single 'queued' ad with the oldest 'last_used' timestamp
			Global user_id := userid
		}
	}
	ad := mysql.query( db, ad_sql, 0 )
	if ad =
	{
		MsgBox, 262160, Post Juice Error, Oh snap! There are no queued ads in the system, 10
		Reload
	}
	StringSplit, ad_array, ad, |
	Global ad_id := ad_array1
	Global account_id := ad_array3
	Global campaign_id := ad_array4
	Global template_id := ad_array5
	Global posting_title := shiftsRegex( ad_array6 )
	Global posting_description := shiftsRegex( ad_array7 )
	Global zip := shiftsRegex( ad_array8 )
	Global use_phone_fields := ad_array9
	Global ad_phone_number := shiftsRegex( ad_array10 )
	Global phone_replies := shiftsRegex( ad_array11 )
	Global email_replies := shiftsRegex( ad_array12)
	Global contact_name := shiftsRegex( ad_array13 )
	Global specific_location := shiftsRegex( ad_array14 )
	Global ok_to_contact := ad_array15
	Global show_on_maps := ad_array16
	Global street := shiftsRegex( ad_array17 )
	Global cross_street := shiftsRegex( ad_array18 )
	Global city := shiftsRegex( ad_array19 )
	Global state := shiftsRegex( ad_array20 )
	Global img_ids := ad_array21
	Global price := shiftsRegex( ad_array26 )
	Global rent := shiftsRegex( ad_array27 )
	Global bedrooms := ad_array28
	Global bathrooms := ad_array28
	Global sqft := ad_array30
	Global housing_type := ad_array31
	Global laundry := ad_array32
	Global parking := ad_array33
	Global furnished := ad_array34
	Global dogs_ok := ad_array35
	Global cats_ok := ad_array36
	; Update 'last_used' fields in DB
	ad_sql = 
	(
		UPDATE pj_web_apper_ads
	    SET ad_last_used = NOW()
		WHERE ID = %ad_array1%
	)
	mysql.query(db, ad_sql, 0)
}

get_template() {
	Global template_id ; Make vars available
	template_sql = ( SELECT template_html,template_phone_number FROM pj_web_apper_templates WHERE ID = %template_id% ) 
	template := mysql.query( db, template_sql, 0 )
	if template =
	{
		MsgBox, 262160, Post Juice Error, Oh snap! The ad template is missing, 10
	} else {
		; Parse into individual vars
		StringSplit, template_array, template, |
		Global template_html := shiftsRegex( template_array1 )
		Global template_phone_number := shiftsRegex( template_array2 )
	}
}

get_campaign() {
	Global campaign_id ; Make vars available
	campaign_sql = ( SELECT campaign_area,campaign_sub_area,campaign_category,campaign_sub_category FROM pj_web_apper_campaigns WHERE ID = %campaign_id%  )
	campaign := mysql.query( db, campaign_sql, 0 )
	if campaign =
	{
		MsgBox, 262160, Post Juice Error, Oh snap! The ad campaign is missing, 10
		Reload
	} else {
		; Parse into individual vars
		StringSplit, campaign_array, campaign, |
		Global area_url := shiftsRegex( campaign_array1 )
		Global sub_area := htmlentitiesEncode( campaign_array2 )
		Global category := htmlentitiesEncode( campaign_array3 )
		Global sub_category := htmlentitiesEncode( campaign_array4 )
	}
}

get_account() {
 	Global App_Client, clients_array ; Make vars available
   ; Get account from db that has the oldest 'last_used' timestamp
	For username, userid in clients_array
	{
		if ( App_Client = username )
		{
			account_sql = ( SELECT ID, claccount_username, claccount_password FROM pj_web_apper_claccounts WHERE claccount_user_id = %userid% ORDER BY claccount_last_used ASC LIMIT 1 )
		}
	}
	account := mysql.query(db, account_sql, 0)
	if account =
	{
		MsgBox, 262160, Post Juice Error, There are no accounts in the system., 10
		Reload
	}
	; Parse the account into individual vars
	StringSplit, account_array, account, |
	global account_id := shiftsRegex(account_array1)
	global account_username := shiftsRegex(account_array2)
	global account_password := shiftsRegex(account_array3)
	; Update account's 'last_used' field in db
	account_sql = 
	(
	    UPDATE pj_web_apper_claccounts
	    SET claccount_last_used = NOW()
	    WHERE ID = %account_array1%
	)
	mysql.query(db, account_sql, 0)
}

get_new_account() {
    ; Get account from db
	newaccount_sql = ( SELECT ID,newaccount_username,newaccount_email_address_domain,newaccount_password,newaccount_firstname,newaccount_lastname FROM pj_web_apper_newaccounts LIMIT 1 )
	newaccount := mysql.query(db, newaccount_sql, 0)
	if newaccount =
	{
		MsgBox, 262160, Post Juice Error, Oh snap! There are no new accounts in the system., 10
		Reload
	}
	; Parse the account into individual vars
	StringSplit, newaccount_array, newaccount, |
	Global newaccount_username := shiftsRegex( newaccount_array2 )
	Global newaccount_usernameRaw := newaccount_array2
	Global newaccount_emailDomain := newaccount_array3
	Global newaccount_pass := shiftsRegex( newaccount_array4 )
	Global newaccount_passRaw := newaccount_array4
	Global newaccount_fname := shiftsRegex( newaccount_array5 )
	Global newaccount_lname := shiftsRegex( newaccount_array6 )
	; Delete account now that it"s been used
	newaccount_sql = 
	(
		DELETE FROM pj_web_apper_newaccounts
		WHERE ID = "%newaccount_array1%"
	)
	mysql.query(db, newaccount_sql, 0)
}

get_phonenumber_by( key ) {
    ; Get phonenumber from db
	phonenumber_sql = ( SELECT ID,phonenumber_phonenumber FROM pj_web_apper_phonenumbers WHERE %key% = 'ready' ORDER BY phonenumber_last_used ASC LIMIT 1 )
	phonenumber := mysql.query(db, phonenumber_sql, 0)
	if phonenumber =
	{
		MsgBox, 262160, Post Juice Error, Oh snap! There are no phone numbers ready in the system., 10
		Reload
	}
	; Parse the account into individual vars
	StringSplit, phonenumber_array, phonenumber, |
	Global phonenumber_id = shiftsRegex( phonenumber_array1 )
	Global phone_number = shiftsRegex( phonenumber_array2 )
	phonenumber_sql = 
	(
	    UPDATE pj_web_apper_phonenumbers
	    SET phonenumber_last_used = NOW()
	    WHERE ID = "%phonenumber_id%"
	)
	mysql.query(db, phonenumber_sql, 0)
}

get_post(){
	post_sql = ( SELECT ID, post_cl_account_id, post_ad_id, post_url FROM pj_web_apper_posts WHERE post_status ='pending' AND DATE_SUB(NOW(), INTERVAL 1 HOUR) > post_posted_on ORDER BY post_posted_on ASC LIMIT 1 )
	post := mysql.query(db, post_sql, 0)
	if post =
	{
		MsgBox, 262160, Post Juice Error, Oh snap! There are no posts to check in the system., 10
		Reload
	}
	; Parse the account into individual vars
	StringSplit, post_array, post, |
	Global post_id := post_array1
	Global post_cl_account_id := post_array2
	Global post_ad_id := post_array3
	Global post_url := post_array4
	ad_sql = ( SELECT ad_title FROM pj_web_apper_ads WHERE ID = %post_ad_id% ) ; get a single 'queued' ad with the oldest 'last_used' timestamp
	ad := mysql.query( db, ad_sql, 0 )
	StringSplit, ad_array, ad, |
	Global ad_title := ad_array1
}




download_attachments() {
	Global img_ids, phonenumber_phonenumber
	Global photo_paths := ""
	first_photo := true
	Loop, Parse, img_ids, `,
	{
		attachment_sql = ( SELECT attachment_file_path, attachment_file_name FROM pj_web_apper_attachments WHERE ID = %A_LoopField% )
		attachment := mysql.query(db, attachment_sql, 0)
		if attachment =
		{
			MsgBox, 262160, Post Juice Error, Oh snap! There are no new accounts in the system., 10
		}
		; Parse the account into individual vars
		StringSplit, attachment_array, attachment, |

		StringReplace, photo_path, attachment_array1, /home/azrto/public_html/pj, http://pj.azrto.info
		photo_url = %photo_path%%attachment_array2%
		Random, imgName, 1000, 10000
		UrlDownloadToFile, %photo_url%, %A_WorkingDir%\%imgName%.jpg ; Download the image
		photo_paths = %photo_paths% "%imgName%.jpg"
		if first_photo
		{
			url = localhost/PHPImageWorkshop/watermark.php?phonenumber=%phonenumber_phonenumber%&filename=%imgName%.jpg&filepath=%A_WorkingDir%
			ieGenerateImage := ComObjCreate("InternetExplorer.Application") ; Open 2'nd IE
			ieGenerateImage.Visible:=True ; Show IE
			ieGenerateImage.Navigate(url)
			WinWaitLoad( ieGenerateImage )
			WinClose, ahk_class IEFrame
			first_photo := false
		}
	}	
}




update_phonenumber_email_status( email_status ) {
	Global phonenumber_id ; Make vars available
	phonenumber_sql = 
	(
	    UPDATE pj_web_apper_phonenumbers
	    SET phonenumber_email_status = "%email_status%"
	    WHERE ID = "%phonenumber_id%"
	)
	mysql.query(db, phonenumber_sql, 0)
}

update_account( key, value ) {
 	Global account_id ; Make vars available
	account_sql = 
	(
	    UPDATE pj_web_apper_claccounts
	    SET %key% = %value%
	    WHERE ID = "%account_id%"
	)
	mysql.query(db, account_sql, 0)
}

update_post_status( status ) {
	Global post_id ; Make vars available
	if ( Status = "ghosted" ) {
		post_sql = 
		(
		    UPDATE pj_web_apper_posts
		    SET post_status = "%status%"
		    WHERE ID = "%post_id%" AND DATE_SUB(NOW(), INTERVAL 3 HOUR) > post_posted_on
		)
	} else {
		post_sql = 
		(
		    UPDATE pj_web_apper_posts
		    SET post_status = "%status%"
		    WHERE ID = "%post_id%"
		)
	}
	mysql.query(db, post_sql, 0)
}




save_post( url ) {
 	Global user_id, account_id, ad_id ; Make vars available
	post_sql = 
	(
	    INSERT INTO pj_web_apper_posts ( post_user_id, post_cl_account_id, post_ad_id, post_url, post_posted_on, post_status ) 
	    VALUES ("%user_id%", "%account_id%", "%ad_id%", "%url%", NOW(), "pending")
	)
	mysql.query(db, post_sql, 0)
}

save_account() {
	; Save newly verfied CL account to the db
	global App_Client, clients_array, emailRaw, passRaw ; Make vars available
	For username, userid in clients_array
	{
		if ( App_Client = username )
		{
			account_sql = 
			(
			    INSERT INTO pj_web_apper_claccounts ( claccount_user_id, claccount_username, claccount_password, claccount_status, claccount_phone_verified, claccount_created_on )
			    VALUES ( "%userid%", "%newaccount_usernameRaw%%newaccount_emailDomain%", "%newaccount_passRaw%", "pending", 0, NOW() )
			)
		}
	}
	result := mysql.query(db, account_sql, 0)
	if result =
	{
		MsgBox, 262160, Post Juice Error, Oh snap! The CL account was not saved., 10
		Reload
	}
}

save_email() {
	global newaccount_usernameRaw, newaccount_emailDomain, newaccount_passRaw ; Make vars available
	emailaccount_sql = 
	(
	    INSERT INTO pj_web_apper_emailaccounts ( emailaccount_username, emailaccount_password, emailaccount_email_address_domain )
	    VALUES ( "%newaccount_usernameRaw%", "%newaccount_passRaw%", "%newaccount_emailDomain%" )
	)
	mysql.query(db, emailaccount_sql, 0)
}

delete_account() {
 	Global account_id ; Make vars available
	account_sql = 
	(
	    DELETE FROM wp_post_juice_pv_accounts
	    WHERE ID = "%account_id%"
	)
	mysql.query(db, account_sql, 0)
}

