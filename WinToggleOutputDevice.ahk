;
; This Script will toogle between the 1st & 3rd Playback devices for Windows
;
Run, mmsys.cpl
WinWait,Sound
ControlSend,SysListView321,{Down} ; Go to the 1st option in the Playback Device list
ControlGet, isEnabled, Enabled,,&Set Default ; Check if it is set as the default option
if(!isEnabled) ; If it is the default option
{
	ControlSend,SysListView321,{Down 2} ; Go down 2 options
}
ControlClick,&Set Default ; Set as the default option
ControlClick,OK
WinWaitClose
SoundPlay, *64
ExitApp ; Close the Application