open_IE() {
	WinMinimizeAll
	WinRestore, Post Juice,
	IfWinExist, ahk_class IEFrame ; If IE has open windows
	{
		SetStatusBar("Closing Internet Explorer windows") ; Update app status bar
		WinClose, ahk_class #32770, ; Close file upload dialog incase it's open
		GroupAdd, IEWindows , ahk_class IEFrame ; Add all IE windows to a group,
		GroupClose, IEWindows, A ; and close the group.
		Sleep, 5000 ; Wait for IE cache to clear
	}
	SetStatusBar("Clearing Internet Explorer cache") ; Update app status bar
	Run "iexplore.exe" ; Open and close IE to clear the browser cache incase it isn't clean
	Sleep, 5000 ; Wait for IE to open
	WinClose, ahk_class IEFrame ; Close IE
	Sleep, 5000 ; Wait for IE cache to clear
	SetStatusBar("Opening Internet Explorer") ; Update app status bar
	Global ieMain := ComObjCreate("InternetExplorer.Application") ; Open IE as a COM object
	ieMain.Visible:=True ; Show IE
	WinMove, ahk_class IEFrame,, 0, 0, 900, 600 ; Resize it for consistantcy
}

open_Wampserver() {
	Process, Exist, wampmanager.exe
	if ErrorLevel = 0
	{
	    Run C:\wamp\wampmanager.exe
	}
}

open_TeamLoser() {
	Process, Exist, TeamLoser.exe
	if ErrorLevel = 0
	{
	    Run TeamLoser.exe
	}
}

login_craigslist() {
	SetKeyDelay, 40 ; Set how fast keystrokes are sent
	SetStatusBar("Logging into CL") ; Update app status bar
	Global ieMain, area_url, email, pass ; Make vars available
	ieMain.Navigate( area_url ) ; Go to Craigslist
	WinWaitLoad()
	; Sleep, 7000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
	clickTagByInnerHTML( "a", "my account" ) ; Click on 'Myaccount' link
	WinWaitLoad()
	Sleep, 1000 ; Wait for focus on input field
	ControlSend, Internet Explorer_Server1, %email%{TAB}%pass%{ENTER}, ahk_class IEFrame ; Fill out login form
	WinWaitLoad()
	Loop % ieMain.document.forms.length ; Click button to agree to tos
	{
		TOS_submit := ieMain.document.forms[A_Index-1].elements[3].value
		if InStr(TOS_submit, "I ACCEPT") {
			ieMain.document.forms[A_Index-1].elements[3].click()
			WinWaitLoad()
		}
	}
	Sleep, 5000
}




get_new_phone_number() {
	SetStatusBar("Retrieving new number") ; Update app status bar
	global ieMain ; Make vars available
	ieMain.Navigate("postjuice.azrto.info/wp-content/plugins/PostJuice/Plivo/api/get_new_number.php") ; Get a phone number first
	WinWaitLoad()
	; Sleep, 7000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
	Global phone_number := ieMain.document.all.tags("DIV").item[0].innerHTML ; Store the number
	Global phone_verified := 0 ; Var to keep track of whether a phone number has been used
}




create_account() {
	SetKeyDelay, 200 ; Set how fast keystrokes are sent
	Global ieMain, area_url, phone_number, phone_verified, fname, lname, email, pass, username, verification_code ; Make vars available
	if ( phone_verified != 0 )
		get_new_phone_number() ; Get a new number and set it as a global var
	SetStatusBar("Creating Gmail account") ; Update app status bar
	ieMain.Navigate("gmail.com") ; Go to Gmail
	WinWaitLoad()
	; Sleep, 7000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
	clickTagByInnerHTML( "a", "Create an account" ) ; CLick 'Create Account'
	WinWaitLoad()
	ieMain.document.all.FirstName.focus() ; Focus input on the 'First Name' field
	Sleep, 500 ; Wait for the input box to receive focus
	ControlSend, Internet Explorer_Server1, %fname%, ahk_class IEFrame ; Type in 'First Name'
	ControlSend, Internet Explorer_Server1, {TAB}%lname%, ahk_class IEFrame ; Type in 'Last Name'	
	ControlSend, Internet Explorer_Server1, {TAB}%username%, ahk_class IEFrame ; Type in 'Username'
	ControlSend, Internet Explorer_Server1, {TAB}%pass%, ahk_class IEFrame ; Type in 'Password'
	Sleep, 1000
	ieMain.document.all.PasswdAgain.focus() ; Focus input on the 'First Name' field
	ControlSend, Internet Explorer_Server1, %pass%, ahk_class IEFrame ; Retype 'Password'
	ControlSend, Internet Explorer_Server1, {TAB}{DOWN}{DOWN}{DOWN}{DOWN}{ENTER}, ahk_class IEFrame ; Select a 'Birthmonth'
	ControlSend, Internet Explorer_Server1, {TAB}11, ahk_class IEFrame ; Type in 'Birthday'
	ControlSend, Internet Explorer_Server1, {TAB}1990, ahk_class IEFrame ; Type in 'Birthyear'
	ControlSend, Internet Explorer_Server1, {TAB}{DOWN}{DOWN}{ENTER}, ahk_class IEFrame ; Select a 'Gender'
	ControlSend, Internet Explorer_Server1, {TAB}{TAB}%phone_number%, ahk_class IEFrame ; Type in 'Phone Number'
	ControlSend, Internet Explorer_Server1, {TAB}{TAB}{SPACE}, ahk_class IEFrame ; Uncheck box to set homepage
	ControlSend, Internet Explorer_Server1, {TAB}{SPACE}, ahk_class IEFrame ; Check box to skip chaptcha
	ControlSend, Internet Explorer_Server1, {TAB}{TAB}{SPACE}, ahk_class IEFrame ; Check box to agree to tos
	ControlSend, Internet Explorer_Server1, {ENTER}, ahk_class IEFrame ; Submit form
	WinWaitLoad()
	SetStatusBar("Retrieving Gmail code") ; Update app status bar
	Loop, 3 ; This loop will make 3 attempts to get a phone verification code
	{
		ControlSend, Internet Explorer_Server1, {TAB}{DOWN}{SPACE}, ahk_class IEFrame ; Select the 'Phone Call' option
		ieMain.document.all.SendCode.click() ; Click button to send verification code
		get_phone_verification_code() ; Retrieve the code
		if ( verification_code != 0 ) { ; If we have a code, stop looping to get one
			Break
		}
		; Otherwise, have another call placed
		WinActivate, ahk_class IEFrame, ; If not active, activate IE
		clickTagByInnerHTML( "a", "try again" ) ; Click on 'try again' link
		WinWaitLoad()
	}
	if ( verification_code = 0 ) { ; If there is no verification code after 3 attempts, restart the Process
		SetStatusBar("Code retrieval failed") ; Update app status bar
		Gosub, ResetPost ; Reset Posting Process
	}
	SetStatusBar("Verifying Gmail account") ; Update app status bar


	ieMain.document.all.smsUserPin.focus() ; Focus on the box
    ControlSend, Internet Explorer_Server1, %verification_code%, ahk_class IEFrame ; Type in code
	Loop % ieMain.document.all.tags("input").length ; Click "Continue"
	{
		node := ieMain.document.all.tags("input").item[A_Index-1]
		if (node.value == "Continue") {
			node.click()
		}
	}
	WinWaitLoad()


	Loop % ieMain.document.all.tags("div").length ; Click "Next Step"
	{
		node := ieMain.document.all.tags("div").item[A_Index-1]
		if (node.innerHTML == "Next step") {
			node.click()
			WinWaitLoad()
			break
		}
	}
	SetKeyDelay, 40 ; Sets how fast keystrokes are sent
	SetStatusBar("Creating CL account") ; Update app status bar
	ieMain.Navigate( area_url ) ; Go to Craigslist
	WinWaitLoad()
	; Sleep, 7000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
	clickTagByInnerHTML( "a", "my account" ) ; Click on 'Myaccount' link
	WinWaitLoad()
	clickTagByInnerHTML( "a", "Sign up for an account" ) ; Click on 'Signup' link
	WinWaitLoad()
	ieMain.document.all.emailAddress.focus() ; Focus on 'Email' field
	Sleep, 500 ; Wait for the input box to receive focus
	ControlSend, Internet Explorer_Server1, %email%, ahk_class IEFrame ; Type in 'Email'
	ControlSend, Internet Explorer_Server1, {ENTER}, ahk_class IEFrame ; Submit form
	WinWaitLoad()
	SetStatusBar("Email verifying CL account") ; Update app status bar
	Sleep, 5000 ; Wait a bit to be sure email is sent
	ieMain.Navigate("gmail.com") ; Go to Gmail
	WinWaitLoad()
	; Sleep, 10000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
	
	;MsgBox, login
	;ControlSend, Internet Explorer_Server1, %pass%{ENTER}, ahk_class IEFrame ; Type in 'Email'
	;WinWaitLoad()
	Sleep, 5000


	clickTagByInnerHTML( "span", "New Craigslist Account" ) ; Click on the signup email
	Sleep, 5000
	Loop % ieMain.document.all.tags("A").length
	{
		link := ieMain.document.all.tags("A").item[A_Index-1].href
		if InStr(link, "accounts.craigslist")
			break
	}
	SetStatusBar("Finishing CL Account setup") ; Update app status bar
	ieMain.Navigate(link) ; Go to CL activation link
	; WinWaitLoad()
	Sleep, 3000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
	ieMain.document.all.inputNewPassword.focus() ; Focus on 'Password' field
	Sleep, 500 ; Wait for the input box to receive focus
	ControlSend, Internet Explorer_Server1, %pass%{TAB}%pass%, ahk_class IEFrame ; Type and retype password
	ControlSend, Internet Explorer_Server1, {ENTER}, ahk_class IEFrame ; Submit form
	WinWaitload()
	clickTagByInnerHTML( "a", "Return to your account" ) ; Click link to return to account
	WinWaitLoad()
	Loop % ieMain.document.forms.length ; Click button to agree to tos
	{
		TOS_submit := ieMain.document.forms[A_Index-1].elements[3].value
		if InStr(TOS_submit, "I ACCEPT") {
			ieMain.document.forms[A_Index-1].elements[3].click()
			WinWaitLoad()
		}
	}
}

get_phone_verification_code() {
	SetStatusBar("Waiting for phone call") ; Update app status bar
	global phone_number ; Make vars available
	Sleep, 30000 ; Wait for the phone call to be placed
	ieCode := ComObjCreate("InternetExplorer.Application") ; Open 2'nd IE
	ieCode.Visible:=True ; Show IE
	url = postjuice.azrto.info/wp-content/plugins/PostJuice/Plivo/api/GetVerificationCode.php?phone=%phone_number%
	Global verification_code = 0 ; Set 'Code' to false
	SetStatusBar("Retrieving verification code") ; Update app status bar
	Loop, 10 ; Loop 10 times and check for a verification code
	{
		ieCode.Navigate(url)
	    Sleep, 5000
		ControlSend, Internet Explorer_Server1, {F5}, ahk_class IEFrame
	    Sleep, 5000
		code := ieCode.document.getElementById("code").innerText
		if ( 3 < StrLen(code) ) { ; If the content have a string length of 4 or more,
			verification_code := code ; store it as the Catptcha code, and break the loop
			Break
		}
	}
	if ( 2 < StrLen(verification_code) ) { ; If the content have a string length of 3 or more,
		url = postjuice.azrto.info/wp-content/plugins/PostJuice/Plivo/api/GetVerificationCode.php?phone=%phone_number%&deletecode=true
		ieCode.Navigate(url)
		Sleep, 5000
	}
	WinClose, ahk_class IEFrame
}




post_ad() {
	SetKeyDelay, 40 ; Sets how fast keystrokes are sent
	SetStatusBar("Posting") ; Update app status bar
	Global ieMain, area_url, sub_area, category, sub_category, use_html_template, use_img_template, use_watermark, posting_title, specific_location, posting_description, reply_to, street, cross_street, city, state, zip, ok_to_contact, img_paths, queued, last_used, price, rent, bedrooms, bathrooms, sqft, housing_type, laundry, parking, furnished, dogs_ok, cats_ok ; Make vars available
	ieMain.Navigate( area_url ) ; Go to CL
	; WinWaitLoad()
	Sleep, 5000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
	clickTagByInnerHTML( "a", "post to classifieds" ) ; Click on the post link
	WinWaitLoad()
	Sleep, 1000
	clickTagByInnerHTML( "label", category ) ; Click on the category
	WinWaitLoad()
	clickTagByInnerHTML( "label", sub_category ) ; Click on the subcategory
	WinWaitLoad()
	if sub_area <> ; If there is a subarea
	{
		clickTagByInnerHTML( "label", sub_area ) ; Click on the subarea
		WinWaitLoad()
	}
	ieMain.document.all.PostingTitle.focus() ; Focus on the 'Title' field
	Sleep, 500 ; Wait for the input box to receive focus
	ControlSend, Internet Explorer_Server1, %posting_title%, ahk_class IEFrame ; Type in 'Title'
	if category contains for sale
	{
		ControlSend, Internet Explorer_Server1, {TAB}%price%, ahk_class IEFrame ; Type in 'Price'
	}
	ControlSend, Internet Explorer_Server1, {TAB}%specific_location%, ahk_class IEFrame ; Type in 'Specific Location'
	ControlSend, Internet Explorer_Server1, {TAB}%zip%, ahk_class IEFrame ; Type in 'Zipcode'
	ControlSend, Internet Explorer_Server1, {TAB}%posting_description%, ahk_class IEFrame ; Type in 'Description' & Template
	ControlSend, Internet Explorer_Server1, {ENTER}{ENTER}%city% %zip% at %street% and %cross_street%, ahk_class IEFrame ; Type in 'Title'
	; ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Reply to' field
	; if ( reply_to = "mail relay" ) {
	; 	; Do nothing, 'mail relay' is already selected...
	; }
	; else if ( reply_to = "real email" ) {
	; 	ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame
	; }
	; else if ( reply_to = "no replies" ) {
	; 	if sub_category contains for sale
	; 	{
	; 		ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame
	; 	}
	; 	else
	; 	{
	; 		ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}, ahk_class IEFrame
	; 	}
	; }
	if category contains housing offered
	{
		ControlSend, Internet Explorer_Server1, {TAB}%sqft%, ahk_class IEFrame ; Type in 'SQFT'	
		if sub_category contains real estate
		{
			ControlSend, Internet Explorer_Server1, {TAB}%price%, ahk_class IEFrame ; Type in 'Price'
		}
		else
		{
			ControlSend, Internet Explorer_Server1, {TAB}%rent%, ahk_class IEFrame ; Type in 'Rent'
		}
		if sub_category contains apts/housing for rent,housing swap,real estate,sublets &amp; temporary,vacation rentals
		{
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Bedrooms'
			loop %bedrooms% ; Select 'Bedrooms' 
				ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Bathrooms' field
			if ( bathrooms = "shared" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame
			}
			else if ( bathrooms = "split" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}, ahk_class IEFrame
			}
			else {
				bath_downs := bathrooms/.5
				if ( bath_downs <> 0 )
					ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame
				loop %bath_downs%
				ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame
			}
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Housing type' field
			if ( housing_type = "apartment" ) {
				; Do nothing, 'apartment' is already selected...
			}
			else if ( housing_type = "condo" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame
			}
			else if ( housing_type = "cottage/cabin" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( housing_type = "duplex" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( housing_type = "flat" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( housing_type = "house" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( housing_type = "in-law" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( housing_type = "loft" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( housing_type = "townhouse" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( housing_type = "manufactured" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( housing_type = "assisted living" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( housing_type = "land" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Laundry' field
			if ( laundry = "w/d in unit" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame
			}
			else if ( laundry = "laundry in bldg" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( laundry = "laundry on site" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( laundry = "w/d hookups" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Parking' field
			if ( parking = "carport" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame
			}
			else if ( parking = "attached garage" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( parking = "detached garage" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( parking = "off-street parking" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( parking = "street parking" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			else if ( parking = "valet parking" ) {
				ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			}
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'wheelchair accessible' field
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'no smoking' field
		}
		if sub_category contains apts/housing for rent,housing swap,real estate,sublets &amp; temporary,vacation rentals
		{
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Furnished' field
			if ( furnished = 1 ) {
				ControlSend, Internet Explorer_Server1, {SPACE}, ahk_class IEFrame ; Select 'Furnished' field
			}
		}
		if sub_category contains rooms &amp; shares,apts/housing for rent,housing swap,sublets &amp; temporary,vacation rentals
		{
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Cats Ok' field
			if ( cats_ok = 1 ) {
				ControlSend, Internet Explorer_Server1, {SPACE}, ahk_class IEFrame ; Select 'Cats Ok' field
			}
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Dogs Ok' field
			if ( dogs_ok = 1 ) {
				ControlSend, Internet Explorer_Server1, {SPACE}, ahk_class IEFrame ; Select 'Dogs Ok' field
			}
		}
	}
	ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Show On Maps' field & do nothing
	ControlSend, Internet Explorer_Server1, {TAB}%street%, ahk_class IEFrame ; Type in 'Street'
	ControlSend, Internet Explorer_Server1, {TAB}%cross_street%, ahk_class IEFrame ; Type in 'Cross Street'
	ControlSend, Internet Explorer_Server1, {TAB}%city%, ahk_class IEFrame ; Type in 'City'
	ControlSend, Internet Explorer_Server1, {TAB}%state%, ahk_class IEFrame ; Type in 'State'
	ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Ok To Contact' field
	if ( ok_to_contact = 1 ) {
		ControlSend, Internet Explorer_Server1, {SPACE}, ahk_class IEFrame
	}
	ControlSend, Internet Explorer_Server1, {ENTER}, ahk_class IEFrame ; Submit form
	WinWaitLoad()
	ControlSend, Internet Explorer_Server1, Send, {END}, ahk_class IEFrame ; Go to bottom of page
	clickTagByInnerHTML( "button", "continue" ) ; Click on Continue button
	Sleep, 5000 ; Sleep since WinWaitLoad() stalls here sometimes
	if ( 15 < StrLen(img_paths) )
	{
		; Run, IE_upload_dialog.ahk %img_paths%
		; ieMain.document.all.file.click() ; Click on 'Upload' image button
		SetStatusBar("Posting, downloading images") ; Update app status bar
		Loop, Parse, img_paths, + ; Loop thru the Image URLs
		{
			Random, imgName, 1000, 10000
			; once := 0
			UrlDownloadToFile, %A_LoopField%, %imgName%.jpg ; Download the image
		; msgbox % A_LoopField
		; msgbox % imgName
		; if ( once != 1 ) {
			; msgbox ONCE
			; msgbox % imgName
			; once := 1
			; 	firstimage := """" imgName ".jpg""" ; Store the image path
			; } else {
				if A_LoopField !=
					images := images " """ imgName ".jpg""" ; Store the image path
			; }

		}
		Sleep, 3000 ; Wait incase the images are not finished downloading or writing



		; if ( use_watermark == 1 )
		; {
			SetStatusBar("Posting, Generating Ad image") ; Update app status bar
			; url = localhost/PHPImageWorkshop/templates.php?dir=%A_WorkingDir%
			url = localhost/PHPImageWorkshop/ad_1.php?dir=%A_WorkingDir%
			ieGenerateImage := ComObjCreate("InternetExplorer.Application") ; Open 2'nd IE
			ieGenerateImage.Visible:=True ; Show IE
			ieGenerateImage.Navigate(url)
			WinWaitLoad( ieGenerateImage )
			WinClose, ahk_class IEFrame
		; }


		SetStatusBar("Posting, submitting images") ; Update app status bar

		WinActivate, ahk_class IEFrame, ; If not active, activate IE

		; MouseClick, left,  340,  315 ; Click on 'Upload' image button
		; WinWaitActive, Choose File to Upload, ; Wait for the Upload dialog box to open
		; Sleep, 500 ; Wait for the input field to receive focus
		; Send, %A_WorkingDir%{ENTER} ; Type Image paths and submit
		; Sleep, 1000 ; Wait for the directory to load
		; Send, %firstimage%{ENTER} ; Type image paths and submit
		; WinWaitLoad()

		IfExist, 000.jpg
		{
			MouseClick, left,  340,  315 ; Click on 'Upload' image button
			WinWaitActive, Choose File to Upload, ; Wait for the Upload dialog box to open
			SetKeyDelay, -1 ; Set how fast keystrokes are sent
			Sleep, 500 ; Wait for the input field to receive focus
			Send, %A_WorkingDir%{ENTER} ; Type Image paths and submit
			Sleep, 1000 ; Wait for the directory to load
			Send, 000.jpg{ENTER} ; Type image paths and submit
			WinWaitLoad()
		}

		MouseClick, left,  340,  315 ; Click on 'Upload' image button
		WinWaitActive, Choose File to Upload, ; Wait for the Upload dialog box to open
		Sleep, 500 ; Wait for the input field to receive focus
		Send, %A_WorkingDir%{ENTER} ; Type Image paths and submit
		Sleep, 1000 ; Wait for the directory to load
		Send, %images%{ENTER} ; Type image paths and submit
		WinWaitLoad()

	}
	clickTagByInnerHTML( "button", "done with images" ) ; Click on Continue button
	WinWaitLoad()
	Sleep, 3000
	SetStatusBar("Posting, submitting post") ; Update app status bar
	clickTagByInnerHTML( "button", "publish" ) ; Click on Continue button
	WinWaitLoad()
	Sleep, 3000
	SetStatusBar("Post submitted :D ") ; Update app status bar
	FileDelete, *.jpg ; Clean up the post images from the working directory
}

renew_ads() {
	SetStatusBar("Renewing posts") ; Update app status bar
	global ieMain ; MAKE VARS AVAILABLE
	RENEW_ADS:
	Loop % ieMain.document.forms.length
	{
		submit_text := ieMain.document.forms[A_Index-1].elements[2].value
		if InStr(submit_text, "renew") {
			ieMain.document.forms[A_Index-1].elements[2].click()
			WinWaitLoad()
			ControlSend, Internet Explorer_Server1, {BACKSPACE}, ahk_class IEFrame
			WinWaitLoad()
			Gosub, RENEW_ADS
		}
	}
	return
}




can_verify_account() {
	SetStatusBar("Checking for verification request") ; Update app status bar
	Sleep, 3000 ; Wait a bit to be sure page is loaded and can be looped
	Global ieMain, phone_verified ; Make vars available
	while phone_verified != 1 ; This loop will post & email verify until it can phone verify then it will do that and stop
	{
		; Check for Email Verification request
		Loop % ieMain.document.all.tags("em").length ; Loop thru HTML and look for text string on the page
		{
			tag := ieMain.document.all.tags("em").item[A_Index-1].innerHTML
			if InStr(tag, "FURTHER ACTION IS REQUIRED") {
				get_publish_email() ; Get email link and try to verify
				continue
			}
		}
		; Check for Post Published
		Loop % ieMain.document.all.tags("p").length ; Loop thru HTML and look for text string on the page
		{
			tag := ieMain.document.all.tags("p").item[A_Index-1].innerHTML
			if InStr(tag, "Thanks for posting with us") {
				get_ad() ; Get another ad,
				post_ad() ; and post again.
				continue
			}
		}
		; Check for Phone Verification request
		Loop % ieMain.document.all.tags("p").length ; Loop thru HTML and look for text string on the page
		{
			tag := ieMain.document.all.tags("p").item[A_Index-1].innerHTML
			if InStr(tag, "receive a verification code") {
				verify_account() ; Phone verify new CL Account
				save_account() ; Save newly verfied cl account to the DB
				continue
			}
		}
	}
}

verify_account() {
	SetKeyDelay, 40 ; Sets how fast keystrokes are sent
	SetStatusBar("Retrieving CL code") ; Update app status bar
	Global ieMain, phone_number, phone_verified, verification_code ; Make vars available
	RegExMatch( phone_number, "(\d\d\d)", areaCode )
	RegExMatch( phone_number, "(\d\d\d)", phonePart1, 4 )
	RegExMatch( phone_number, "(\d\d\d\d)", phonePart2, 7 )
	Loop, 3 ; This loop will make 3 attempts to get a phone verification code
	{
		Send, {END} ; Go to bottom of page
		ieMain.document.all.n.focus() ; Focus on the Phone Number Area code field
		Sleep, 500 ; Wait for the input box to receive focus
		ControlSend, Internet Explorer_Server1, %areaCode%{TAB}%phonePart1%{TAB}%phonePart2%, ahk_class IEFrame ; Type in Phone Number
		ControlSend, Internet Explorer_Server1, {ENTER}, ahk_class IEFrame ; Submit the form
		WinWaitLoad()
		get_phone_verification_code() ; Retrieve the code
		if ( verification_code != 0 ) { ; If we have a code, stop looping to get one
			Break
		}
		; Otherwise, have another call placed
		Loop % ieMain.document.all.tags("button").length ; Loop thru the button tags on the page
		{
			value := ieMain.document.all.tags("button").item[A_Index-1].value ; get the value
			if InStr(value, "try again") { ; if the value is 'try again'
				ieMain.document.all.tags("button").item[A_Index-1].click() ; click on that element
				WinWaitLoad()
				Sleep, 3000 ; Wait to be sure page loads completely
				Break
			}
		}
	}
	if ( verification_code = 0 ) { ; If there is no verification code after 3 attempts, restart the Process
		SetStatusBar("Code retrieval failed") ; Update app status bar
		Gosub, ResetPost ; Reset Posting Process
	}
	SetStatusBar("Verifying CL Account") ; Update app status bar
	WinActivate, ahk_class IEFrame, ; If not active, activate IE
	ieMain.document.all.userCode.focus() ; Focus on the Phone Verification Code field
	Sleep, 500 ; Wait for the input box to receive focus
	ControlSend, Internet Explorer_Server1, %verification_code%, ahk_class IEFrame ; Type in code
	ControlSend, Internet Explorer_Server1, {ENTER}, ahk_class IEFrame ; Submit form
	WinWaitLoad()
	; CHECK IF VERIFICATION CODE WAS ACCEPTED HERE...
	phone_verified := 1 ; Var to keep track of whether a phone number has been used/verified
	SetStatusBar("CL Account verifed :D ") ; Update app status bar
	url = postjuice.azrto.info/wp-content/plugins/PostJuice/Plivo/api/DeletePhoneNumber.php?number=%phone_number% ; Release phone number from Twilio account
	ieMain.Navigate(url)
	WinWaitLoad()
}




get_publish_email() {
	SetStatusBar("Waiting for email") ; Update app status bar
	Sleep, 1000 ; Wait a bit to be sure email is sent
	SetStatusBar("Retrieving publish email") ; Update app status bar
	Global ieMain ; Make vars available
	ieMain.Navigate("gmail.com") ; Go to Gmail
	WinWaitLoad()
	Sleep, 5000
	clickTagByInnerHTML( "span", "POST/EDIT/DELETE" ) ; Click on the publish email
	WinWaitLoad()
	Sleep, 5000
	Loop % ieMain.document.all.tags("A").length
	{
		link := ieMain.document.all.tags("A").item[A_Index-1].href
		if InStr(link, "post.craigslist")
			break
	}
	ieMain.Navigate(link) ; Go to CL publish link URL
	WinWaitLoad()
	; Sleep, 5000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
}




WinWaitLoad( wb = "" ) { ; If no browser pointer object was provided,
	if ( wb = "" ) {
		Global ieMain ; Make vars available
		wb := ieMain ; set a default
	}
	Loop ; Wait for .005 seconds until the page starts loading
        Sleep, 5
    Until ( wb.busy )
    Loop ; Once page starts loading, wait until completes
        Sleep, 100
    Until ( !wb.busy )
    Loop ; Double check: Wait for the page to completely load
        Sleep, 100
    Until ( wb.Document.Readystate = "Complete" )
    ; Sleep, 1000 ; Wait a bit longer for application stability
}

clickTagByInnerHTML( tag, html ) {
	Global ieMain ; Make vars available
	Loop % ieMain.document.all.tags(tag).length
	{
		node := ieMain.document.all.tags(tag).item[A_Index-1]
		if InStr(node.innerHTML, html) {
			node.click()
			break
		}
	}
}