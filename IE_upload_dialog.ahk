SetKeyDelay, -1 ; Sets how fast keystrokes are sent

img_paths = %1% ; Get the Image URLs

Loop, Parse, img_paths, + ; Loop thru the Image URLs
{
	UrlDownloadToFile, %A_LoopField%, 000%A_index%.jpg ; Download the image
	; images := images """000" A_index ".jpg"" " ; Store the image path
	if A_LoopField !=
		images := images """000" A_index ".jpg"" " ; Store the image path
}
;MsgBox, %A_WorkingDir%
;MsgBox, %images%
; ControlSetText, Edit1, %A_WorkingDir%, ahk_class #32770, ; Type in the working directory
; ControlSend, Edit1, {ENTER}, ahk_class #32770, ; Move to the working directory
;WinActivate, ahk_class #32770, ; Activate file upload dialog
;WinWaitActive, ahk_class #32770,
Send, %A_WorkingDir%{ENTER} ; Type image paths and submit
Sleep, 100
Send, %images%{ENTER} ; Type image paths and submit

; ControlSetText, Edit1, %A_WorkingDir%, ahk_class #32770, ; Type in the working directory
; ControlSend, Edit1, {ENTER}, ahk_class #32770, ; Move to the working directory
; ControlSetText, Edit1, %images%, ahk_class #32770, ; Type image paths
; ControlSend, Edit1, {ENTER}, ahk_class #32770, ; Submit