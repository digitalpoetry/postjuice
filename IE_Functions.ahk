open_IE() {
	WinMinimizeAll
	WinClose, ahk_class #32770, ; Close file upload dialog incase it's open
	WinRestore, Post Juice,
	IfWinExist, ahk_class IEFrame ; If IE has open windows
	{
		SetStatusBar("Closing Internet Explorer windows") ; Update app status bar
		GroupAdd, IEWindows , ahk_class IEFrame ; Add all IE windows to a group,
		GroupClose, IEWindows, A ; and close the group.
		Sleep, 2000 ; Wait for IE cache to clear
	}
	SetStatusBar("Clearing Internet Explorer cache") ; Update app status bar
 	Run "iexplore.exe" ; Open and close IE to clear the browser cache incase it isn't clean
 	Sleep, 3000 ; Wait for IE to open
 	WinClose, ahk_class IEFrame ; Close IE
	Process, WaitClose, iexplore.exe
	SetStatusBar("Opening Internet Explorer") ; Update app status bar
	Global ieMain := ComObjCreate("InternetExplorer.Application") ; Open IE as a COM object
	ieMain.Visible:=True ; Show IE
	WinMove, ahk_class IEFrame,, 0, 0, 900, 600 ; Resize it for consistantcy
	Sleep, 3000 ; Wait for IE
}

open_Wampserver() {
	Process, Exist, wampmanager.exe
	if ErrorLevel = 0
	{
	    Run C:\wamp\wampmanager.exe
	}
}

open_TeamLoser() {
	Process, Exist, TeamLoser.exe
	if ErrorLevel = 0
	{
	    Run TeamLoser.exe
	}
}




login_craigslist() {
	Global ieMain, area_url, account_username, account_password ; Make vars available
	SetStatusBar("Logging into CL") ; Update app status bar
	SetKeyDelay, 40 ; Set how fast keystrokes are sent
	WinNavigate( area_url )
	clickElement( "innerhtml", "my account", "a" ) ; Click on 'Myaccount' link
	typeTextByName( account_username, "inputEmailHandle" )
	typeTextByName( account_password, "inputPassword" )
	clickElement( "innerhtml", "Log In", "button" )
}




make_gmail_account() {
	Global ieMain, area_url, phone_number, newaccount_fname, newaccount_lname, newaccount_pass, newaccount_username, verification_code ; Make vars available
	SetStatusBar("Creating Gmail account") ; Update app status bar
	SetKeyDelay, 150 ; Set how fast keystrokes are sent
	get_phonenumber_by( "phonenumber_email_status" ) ; Get a new number and set it as a global var
	WinNavigate( "gmail.com" ) ; Go to gmail.com
	Sleep, 2000
	clickElement( "innerhtml", "Create an account", "a" ) ; CLick 'Create Account'
	Sleep, 2000
	typeTextByName( newaccount_fname, "FirstName" ) ; Type in 'First Name'
	Sleep, 2000
	ControlSend, Internet Explorer_Server1, {TAB}%newaccount_lname%, ahk_class IEFrame ; Type in 'Birthday'
	Sleep, 2000
	ControlSend, Internet Explorer_Server1, {TAB}%newaccount_username%, ahk_class IEFrame ; Type in 'Birthday'
	Sleep, 2000
	ControlSend, Internet Explorer_Server1, {TAB}%newaccount_pass%, ahk_class IEFrame ; Type in 'Birthday'
	Sleep, 2000
	ControlSend, Internet Explorer_Server1, {TAB}%newaccount_pass%, ahk_class IEFrame ; Type in 'Birthday'
	Sleep, 2000
    ; typeTextByName( newaccount_lname, "LastName" )
    ; typeTextByName( newaccount_username, "GmailAddress" )
    ; typeTextByName( newaccount_pass, "Passwd" )
    ; typeTextByName( newaccount_pass, "PasswdAgain" )
	ControlSend, Internet Explorer_Server1, {TAB}{DOWN}, ahk_class IEFrame ; Type in 'Birthday'
	Sleep, 2000
	Random, birthmonth, 1, 12
	Loop, % birthmonth {
		ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame ; Type in 'Birthday'	
		Sleep, 100
	}
	Sleep, 500
	Send, {ENTER}
	Sleep, 500
	Random, birthday, 1, 28
	ControlSend, Internet Explorer_Server1, {TAB}%birthday%, ahk_class IEFrame ; Type in 'Birthday'
	Sleep, 2000
	; typeTextByName( birthday, "BirthDay" )
	Random, birthyear, 1964, 1992
 	ControlSend, Internet Explorer_Server1, {TAB}%birthyear%, ahk_class IEFrame ; Type in 'Birthday'
	Sleep, 2000
	; typeTextByName( birthyear, "BirthYear" )
	ControlSend, Internet Explorer_Server1, {TAB}{DOWN}, ahk_class IEFrame ; Type in 'Birthday'
	Sleep, 2000
	Random, gender, 1, 3
	Loop, % gender {
		ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame ; Type in 'Birthday'	
		Sleep, 100
	}
	Sleep, 500
	Send, {ENTER}
	Sleep, 500
 	ControlSend, Internet Explorer_Server1, {TAB}{TAB}%phone_number%, ahk_class IEFrame ; Type in 'Birthday'
	Sleep, 2000
	; typeTextByName( phone_number, "RecoveryPhoneNumber" )
	clickElement( "name", "SkipCaptcha" )
	Sleep, 2000
	clickElement( "name", "TermsOfService" )
	Sleep, 2000
	clickElement( "name", "submitbutton" )
	Sleep, 2000
	SetStatusBar( "Retrieving Gmail code" ) ; Update app status bar
	Loop, { ; This will loop until a code is accepted
		Loop, 3 { ; This loop will make 3 attempts to get a Gmail verification code
			Loop, { ; This will loop until a phonenumber is accepted
				clickElement( "value", "callPhone", "input" )
	Sleep, 2000
				clickElement( "name", "SendCode" ) ; Click button to send verification code
	Sleep, 2000
				; Check for too many verification attempts / already used phone number
				if existsElement( "innerhtml", "This phone number has already", "span" ) {
					SetStatusBar("Using another Phone Number") ; Update app status bar
					update_phonenumber_email_status( "not verified" ) ; Update current phone number status
	Sleep, 2000
					get_phonenumber_by( "phonenumber_email_status" ) ; Get a new number and set it as a global var
	Sleep, 2000
					typeTextByName( "{Ctrl Down}a{Ctrl Up}{DELETE}", "deviceAddress" )
	Sleep, 2000
					ControlSend, Internet Explorer_Server1, %phone_number%, ahk_class IEFrame
	Sleep, 2000
					; typeTextByName( phone_number, "deviceAddress" )
					Sleep, 2000
				} else {
					Break
				}
			}
			get_verification_code() ; Retrieve the code
			if ( verification_code != 0 ) { ; If we have a code, stop looping to get one
				Break
			}
			; Otherwise, have another call placed
			WinActivate, ahk_class IEFrame, ; If not active, activate IE
	Sleep, 2000
		}
		if ( verification_code = 0 ) { ; If there is no verification code after 3 attempts, restart the Process
			SetStatusBar("Code retrieval failed") ; Update app status bar
			Gosub, ResetPost ; Reset Posting Process
		}
		SetStatusBar("Verifying Gmail account") ; Update app status bar
		typeTextByName( verification_code, "smsUserPin" ) ; Type the verification code
	Sleep, 2000
		clickElement( "value", "Continue", "input" ) ; Click "Continue"
	Sleep, 2000
		if existsElement( "innerhtml", "Next step", "div" ) {
			SetStatusBar("Finishing Gmail account") ; Update app status bar
			clickElement( "innerhtml", "Next step", "div" ) ; Click "Next Step"
			Sleep, 9000
			if existsElement( "value", "Continue to Gmail", "input" ) {
				clickElement( "value", "Continue to Gmail", "input" )
				update_phonenumber_email_status( "verified" ) ; Update current phone number status
				save_email()
				Sleep, 9000
				Break
			} else {
				MsgBox, 262160, Post Juice Error, Oh snap! There is an issue with Gmail., 999
				Reload
			}
		} else {
			clickElement( "innerhtml", "try again", "a" )
	Sleep, 2000
	Sleep, 2000
		}
	}
}

create_account() {
	Global ieMain, area_url, phonenumber_phonenumber, phone_verified, fname, lname, email, pass, username, verification_code ; Make vars available
	SetKeyDelay, 40 ; Sets how fast keystrokes are sent
	SetStatusBar("Creating CL account") ; Update app status bar
	ieMain.Navigate( area_url ) ; Go to Craigslist
	Sleep, 5000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
	clickTagByInnerHTML( "a", "my account" ) ; Click on 'Myaccount' link
	WinWaitLoad()
	clickTagByInnerHTML( "a", "Sign up for an account" ) ; Click on 'Signup' link
	WinWaitLoad()
	ieMain.document.all.emailAddress.focus() ; Focus on 'Email' field
	Sleep, 500 ; Wait for the input box to receive focus
	ControlSend, Internet Explorer_Server1, %email%, ahk_class IEFrame ; Type in 'Email'
	ControlSend, Internet Explorer_Server1, {ENTER}, ahk_class IEFrame ; Submit form
	WinWaitLoad()
	SetStatusBar("Waiting for CL email") ; Update app status bar
	Sleep, 10000 ; Wait a bit to be sure email is sent
	SetStatusBar("Email verifying CL account") ; Update app status bar
	ieMain.Navigate("gmail.com") ; Go to Gmail
	Sleep, 10000 ; Wait a bit since WinWaitLoad() works poorly here
	While, StrLen(link) < 10 {
		clickTagByInnerHTML( "span", "New Craigslist Account" ) ; Click on the signup email
		Sleep, 5000 ; Wait a bit to avoid IE navigation warning
		Loop % ieMain.document.all.tags("A").length
		{
			href := ieMain.document.all.tags("A").item[A_Index-1].href
			If ( InStr(href, "accounts.craigslist") ) {
				link := href
				break
			}
		}
	}
	SetStatusBar("Finishing CL Account setup") ; Update app status bar
	ieMain.Navigate(link) ; Go to CL activation link
	Sleep, 5000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
	ieMain.document.all.inputNewPassword.focus() ; Focus on 'Password' field
	Sleep, 500 ; Wait for the input box to receive focus
	ControlSend, Internet Explorer_Server1, %pass%{TAB}%pass%, ahk_class IEFrame ; Type and retype password
	ControlSend, Internet Explorer_Server1, {ENTER}, ahk_class IEFrame ; Submit form
	WinWaitload()
	clickTagByInnerHTML( "a", "Return to your account" ) ; Click link to return to account
	WinWaitLoad()
	Loop % ieMain.document.forms.length ; Click button to agree to tos
	{
		TOS_submit := ieMain.document.forms[A_Index-1].elements[3].value
		if InStr(TOS_submit, "I ACCEPT") {
			ieMain.document.forms[A_Index-1].elements[3].click()
			WinWaitLoad()
		}
	}
}

get_verification_code() {
	global phone_number ; Make vars available
	SetStatusBar("Waiting for phone call") ; Update app status bar
	Sleep, 30000 ; Wait for the phone call to be placed
	SetStatusBar("Retrieving verification code") ; Update app status bar
	ieCode := ComObjCreate("InternetExplorer.Application") ; Open 2'nd IE
	ieCode.Visible:=True ; Show IE
	url = postjuice.net/wp-content/plugins/PostJuice/Twilio/ubot/GetVerificationCode.php?phone=%phone_number%
	Global verification_code = 0 ; Set 'Code' to false
	Loop, 10 ; Loop 10 times and check for a verification code
	{
	    WinNavigate( url, ieCode )
	    Sleep, 1000
		code := ieCode.document.getElementById("code").innerText
		if ( 2 < StrLen(code) ) { ; If the content have a string length of 4 or more,
			verification_code := code ; store it , and break the loop
			url = postjuice.net/wp-content/plugins/PostJuice/Twilio/ubot/GetVerificationCode.php?phone=%phone_number%&deletecode=true
	    	WinNavigate( url, ieCode )
			Break
		}
	}
	WinClose, ahk_class IEFrame
}




post_ad() {
	Global ieMain, area_url,sub_area,category,sub_category,posting_title,posting_description,zip,use_phone_fields,ad_phone_number,phone_replies,email_replies,contact_name,specific_location,ok_to_contact,show_on_maps,street,cross_street,city,state,img_ids,price,rent,bedrooms,bathrooms,sqft,housing_type,laundry,parking,furnished,dogs_ok,cats_ok,template_html,template_phone_number,photo_paths ; Make vars available
	SetStatusBar("Posting") ; Update app status bar
	SetKeyDelay, 40 ; Sets how fast keystrokes are sent
	ieMain.Navigate( area_url ) ; Go to CL
	Sleep, 5000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
	clickElement( "innerhtml", "post to classifieds", "a" ) ; Click on the post link
	clickElement( "innerhtml", category, "label" ) ; Click on the category
	clickElement( "innerhtml", sub_category, "label" ) ; Click on the subcategory
	if sub_area <> ; If there is a subarea
		clickElement( "innerhtml", sub_area, "label" ) ; Click on the subarea
	typeTextByName( posting_title, "PostingTitle" )
    typeTextByName( specific_location, "GeographicArea" )
	typeTextByName( zip, "postal" )
    typeTextByName( posting_description, "PostingBody" )d Meh <3
    typeTextByName( template_html, "PostingBody" )
	if category contains housing offered
	{
	    typeTextByName( "{DELETE}", "Sqft" )
	    typeTextByName( sqft, "Sqft" ) ; Type in 'Sqft'
		if sub_category contains real estate
		{
			typeTextByName( price, "Ask" ) ; Type in 'Price'
		}
		else
		{
			typeTextByName( rent, "Ask" ) ; Type in 'Rent'
		}

		if sub_category contains apts/housing for rent,housing swap,real estate,sublets &amp; temporary,vacation rentals
		{
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Bedrooms'
			loop %bedrooms% ; Select 'Bedrooms' 
				ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame
			

			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Bathrooms' field
			bath_downs := bathrooms/.5
			if ( bath_downs <> 0 )
				ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame
			loop %bath_downs%
				ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame


			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Housing type' field
			ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}{DOWN}{DOWN}{DOWN}, ahk_class IEFrame
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Laundry' field
			ControlSend, Internet Explorer_Server1, {DOWN}, ahk_class IEFrame
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Parking' field
			ControlSend, Internet Explorer_Server1, {DOWN}{DOWN}, ahk_class IEFrame

			ControlSend, Internet Explorer_Server1, {TAB}{TAB}{TAB}, ahk_class IEFrame ; Move to 'Furnished' field
			if ( furnished = 1 ) {
				ControlSend, Internet Explorer_Server1, {SPACE}, ahk_class IEFrame ; Select 'Furnished' field
			}
		}
		if sub_category contains rooms &amp; shares,apts/housing for rent,housing swap,sublets &amp; temporary,vacation rentals
		{
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Cats Ok' field
			if ( cats_ok = 1 ) {
				ControlSend, Internet Explorer_Server1, {SPACE}, ahk_class IEFrame ; Select 'Cats Ok' field
			}
			ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Dogs Ok' field
			if ( dogs_ok = 1 ) {
				ControlSend, Internet Explorer_Server1, {SPACE}, ahk_class IEFrame ; Select 'Dogs Ok' field
			}
		}
	}
	ControlSend, Internet Explorer_Server1, {TAB}, ahk_class IEFrame ; Move to 'Show On Maps' field & do nothing
	ControlSend, Internet Explorer_Server1, {TAB}%street%, ahk_class IEFrame ; Type in 'Street'
	ControlSend, Internet Explorer_Server1, {TAB}%cross_street%, ahk_class IEFrame ; Type in 'Cross Street'
	ControlSend, Internet Explorer_Server1, {TAB}%city%, ahk_class IEFrame ; Type in 'City'
	ControlSend, Internet Explorer_Server1, {TAB}%state%, ahk_class IEFrame ; Type in 'State'
	ControlSend, Internet Explorer_Server1, {ENTER}, ahk_class IEFrame ; Submit form
	WinWaitLoad()
	Sleep, 3000
	ControlSend, Internet Explorer_Server1, Send, {END}, ahk_class IEFrame ; Go to bottom of page
	clickTagByInnerHTML( "button", "continue" ) ; Click on Continue button
	clickTagByInnerHTML( "button", "continue" ) ; Click on Continue button
	WinWaitLoad()
	if ( 7 < StrLen(photo_paths) ) {
		Loop 
		{
			; MouseClick, left, 299, 378 ; Click on 'Upload' image button
			MouseClick, left, 160, 320 ; Click on 'Upload' image button
			Sleep, 1000
			IfWinExist, Choose File to Upload 
			{
				Break
			}
		}
		WinWaitActive, Choose File to Upload, ; Wait for the Upload dialog box to open
		Sleep, 500 ; Wait for the input field to receive focus
		Send, %A_WorkingDir%{ENTER} ; Type Image paths and submit
		Sleep, 1000 ; Wait for the directory to load
		Send, %photo_paths%{ENTER} ; Type image paths and submit
		Sleep, 20000
	}
	clickTagByInnerHTML( "button", "done with images" ) ; Click on Continue button
	WinWaitLoad()
	SetStatusBar("Posting, submitting post") ; Update app status bar
	waitElement( "innerhtml", "publish", "button" )
	waitElement( "innerhtml", "publish", "button" )
	clickTagByInnerHTML( "button", "publish" ) ; Click on Continue button
	WinWaitLoad()
	SetStatusBar("Post submitted :D ") ; Update app status bar
	FileDelete, *.jpg ; Clean up the post images from the working directory
	Sleep, 10000
}

renew_ads() {
	SetStatusBar("Renewing posts") ; Update app status bar
	global ieMain ; MAKE VARS AVAILABLE
	RENEW_ADS:
	Loop % ieMain.document.forms.length
	{
		submit_text := ieMain.document.forms[A_Index-1].elements[2].value
		if InStr(submit_text, "renew") {
			ieMain.document.forms[A_Index-1].elements[2].click()
			WinWaitLoad()
			ControlSend, Internet Explorer_Server1, {BACKSPACE}, ahk_class IEFrame
			WinWaitLoad()
			Gosub, RENEW_ADS
		}
	}
	return
}




can_verify_account() {
	SetStatusBar("Checking for verification request") ; Update app status bar
	Sleep, 3000 ; Wait a bit to be sure page is loaded and can be looped
	Global ieMain, phone_verified ; Make vars available
	; while phone_verified != 1 ; This loop will post & email verify until it can phone verify then it will do that and stop
	Loop,
	{
		; Check for Post Published
		Loop % ieMain.document.all.tags("h4").length ; Loop thru HTML and look for text string on the page
		{
			tag := ieMain.document.all.tags("h4").item[A_Index-1].innerHTML
			if InStr(tag, "Thanks for posting with us") {
				; Save Posted Ad
				Loop % ieMain.document.all.tags("a").length ; Loop thru HTML and look for text string on the page
				{
					tag := ieMain.document.all.tags("a").item[A_Index-1].href
					if InStr(tag, "html") {
						save_post( tag )
						Break 3
					}
				}
			}
		}
		; Check for Email Verification request
		Loop % ieMain.document.all.tags("em").length ; Loop thru HTML and look for text string on the page
		{
			tag := ieMain.document.all.tags("em").item[A_Index-1].innerHTML
			if InStr(tag, "FURTHER ACTION IS REQUIRED") {
				get_publish_email() ; Get email link and try to verify
				Break
			}
		}
		; Check for Phone Verification request
		Loop % ieMain.document.all.tags("strong").length ; Loop thru HTML and look for text string on the page
		{
			tag := ieMain.document.all.tags("strong").item[A_Index-1].innerHTML
			if InStr(tag, "Phone Verification is required") {
				verify_account() ; Phone verify new CL Account
				; save_account() ; Save newly verfied cl account to the DB
				Break
			}
		}
	}
}

verify_account() {
	Global ieMain, phone_number, phone_verified, verification_code ; Make vars available
	SetStatusBar("Retrieving CL code") ; Update app status bar
	get_phonenumber_by( "phonenumber_cl_status" ) ; Get a new number and set it as a global var
	SetKeyDelay, 80 ; Sets how fast keystrokes are sent
	RegExMatch( phone_number, "(\d\d\d)", areaCode )
	RegExMatch( phone_number, "(\d\d\d)", phonePart1, 4 )
	RegExMatch( phone_number, "(\d\d\d\d)", phonePart2, 7 )
	Loop, 3 ; This loop will make 3 attempts to get a phone verification code
	{
		Send, {END} ; Go to bottom of page
		ieMain.document.all.n.focus() ; Focus on the Phone Number Area code field
		Sleep, 500 ; Wait for the input box to receive focus
		ControlSend, Internet Explorer_Server1, %areaCode%{TAB}%phonePart1%{TAB}%phonePart2%, ahk_class IEFrame ; Type in Phone Number
		ControlSend, Internet Explorer_Server1, {ENTER}, ahk_class IEFrame ; Submit the form
		WinWaitLoad()
		get_verification_code() ; Retrieve the code
		if ( verification_code != 0 ) { ; If we have a code, stop looping to get one
			Break
		}
		; Otherwise, have another call placed
		Loop % ieMain.document.all.tags("button").length ; Loop thru the button tags on the page
		{
			value := ieMain.document.all.tags("button").item[A_Index-1].value ; get the value
			if InStr(value, "try again") { ; if the value is 'try again'
				ieMain.document.all.tags("button").item[A_Index-1].click() ; click on that element
				WinWaitLoad()
				; Sleep, 3000 ; Wait to be sure page loads completely
				Break
			}
		}
	}
	if ( verification_code = 0 ) { ; If there is no verification code after 3 attempts, restart the Process
		SetStatusBar("Code retrieval failed") ; Update app status bar
		Gosub, ResetPost ; Reset Posting Process
	}
	SetStatusBar("Verifying CL Account") ; Update app status bar
	WinActivate, ahk_class IEFrame, ; If not active, activate IE
	ieMain.document.all.userCode.focus() ; Focus on the Phone Verification Code field
	Sleep, 500 ; Wait for the input box to receive focus
	ControlSend, Internet Explorer_Server1, %verification_code%, ahk_class IEFrame ; Type in code
	ControlSend, Internet Explorer_Server1, {ENTER}, ahk_class IEFrame ; Submit form
	WinWaitLoad()
	; CHECK IF VERIFICATION CODE WAS ACCEPTED HERE...
	phone_verified := 1 ; Var to keep track of whether a phone number has been used/verified
	SetStatusBar("CL Account verifed :D ") ; Update app status bar
	update_account( "claccount_phone_verified", "1" ) ; Update the account in the database
	Sleep, 10000 ; Wait so that the screen may be read
}

get_publish_email() {
	Global ieMain, account_username, account_password ; Make vars available
	SetStatusBar("Waiting for email") ; Update app status bar
	Sleep, 10000 ; Wait a bit to be sure email is sent
	SetStatusBar("Retrieving publish email") ; Update app status bar
	WinNavigate( "gmail.com" ) ; Go to gmail.com
	typeTextByName( account_username, "Email" )
	typeTextByName( account_password, "Passwd" )
	clickElement( "name", "signIn" )
	if ( existsElement( "innerhtml", "Account has been disabled", "h1" ) ) {
		delete_account()
		Gosub, MenuOptionsResetProcess
	}
	WinWaitLoad()
	if ( existsElement( "value", "Skip", "input" ) ) {
		clickElement( "value", "Skip", "input" )
		WinWaitLoad()
	}
	if ( existsElement( "value", "no thanks", "input" ) ) {
		clickElement( "value", "Skip", "input" )
		WinWaitLoad()
	}
	While, StrLen(link) < 10 {
		clickTagByInnerHTML( "span", "POST/EDIT/DELETE" ) ; Click on the signup email
		Sleep, 5000 ; Wait a bit to avoid IE navigation warning
		Loop % ieMain.document.all.tags("a").length
		{
			href := ieMain.document.all.tags("a").item[A_Index-1].href
			If ( InStr(href, "post.craigslist") ) {
				link := href
				Break
			}
		}
	}
	ieMain.Navigate(link) ; Go to CL publish link URL
	Sleep, 5000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
}




check_post_status() {
	global ieMain, post_id, post_cl_account_id, post_ad_id, post_url, ad_title ; MAKE VARS AVAILABLE
	ieMain.Navigate(post_url) ; Go to CL publish link URL
	Sleep, 5000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
	if ( existsElement( "innerhtml", "This posting has been flagged for removal.", "h2" ) ) {
		update_post_status("flagged")
	} Else {
		RegExMatch( post_url, "[^\d]*", matches )
		StringReplace, url, matches, org, org/search
		url = %url%?query=%ad_title%&sale_date=-&srchType=T
		ieMain.Navigate(url) ; Go to CL publish link URL
		Sleep, 5000 ; Always Sleep  After using 'Navagate' instead if WinWaitLoad
		live := false
		Loop % ieMain.document.all.tags("a").length
		{
			href := ieMain.document.all.tags("a").item[A_Index-1].href
			If ( InStr(post_url, href) ) {
				live := true
				Break
			}
		}
		if ( live = true ) {
			status := live
		} else {
			status := ghosted
		}
		update_post_status(status)
	}
}




WinNavigate( url, wb = "" ) { ; If no browser pointer object was provided,
	if ( wb = "" ) {
		Global ieMain ; Make vars available
		wb := ieMain ; set a default
	}
	wb.Navigate( url )
    Sleep, 7000 ; Wait a bit longer for application stability
}

WinWaitLoad( wb = "" ) { ; If no browser pointer object was provided,
	if ( wb = "" ) {
		Global ieMain ; Make vars available
		wb := ieMain ; set a default
	}
	while ( wb.busy And A_index <= 50 ) {
		Sleep 100
	}
}

clickTagByInnerHTML( tag, html ) {
	Global ieMain ; Make vars available
	Loop % ieMain.document.all.tags(tag).length
	{
		node := ieMain.document.all.tags(tag).item[A_Index-1]
		if InStr(node.innerHTML, html) {
			node.click()
			break
		}
	}
	WinWaitLoad()
}

clickElement( selector, value, tag = "" ) { ; SELECTORS: value, name, id, innerhtml, innertext
	Global ieMain ; Make vars available
	While ( !existsElement( selector, value, tag ) or wb.busy )
		Sleep, 100
	if ( selector = "id" ) {
		ieMain.document.getElementById(value)[0].click()
	} else if ( selector = "name" ) {
		ieMain.document.getElementsByName(value)[0].click()
	} else {
		Loop % ieMain.document.all.tags(tag).length {
			try {
				node := ieMain.document.all.tags(tag).item[A_Index-1]
				if ( selector = "innerhtml" ) {
					node_value := node.innerHTML
				} else if ( selector = "innertext" ) {
					node_value := node.innerText
				} else if ( selector = "value" ) {
					node_value := node.value
				} else if ( selector = "class" ) {
					node_value := node.className
				}
			} catch {
				Break
			}
			if InStr(node_value, value) {
				node.click()
			}
		}
	}
	WinWaitLoad()
}

existsElement( selector, value, tag = "" ) { ; SELECTORS: value, name, id, innerhtml, innertext
	Global ieMain ; Make vars available
	exists := false
	if ( selector = "id" ) {
		if ieMain.document.getElementById(value)[0] {
			exists := true
		}
	} else if ( selector = "name" ) {
		if ieMain.document.getElementsByName(value)[0] {
			exists := true
		}
	} else {
		Loop % ieMain.document.all.tags(tag).length {
			try {
				node := ieMain.document.all.tags(tag).item[A_Index-1]
				if ( selector = "innerhtml" ) {
					node_value := node.innerHTML
				} else if ( selector = "innertext" ) {
					node_value := node.innerText
				} else if ( selector = "value" ) {
					node_value := node.value
				} else if ( selector = "class" ) {
					node_value := node.className
				}
			} catch {
				Break
			}
			if InStr(node_value, value) {
				exists := true
				Break
			}
		}
	}
	return % exists
}

waitElement( selector, value, tag = "" ) { ; SELECTORS: value, name, id, innerhtml, innertext
	Loop, 
	{
		if existsElement( selector, value, tag )
		{
			Break
		}
		Sleep, 300
	}
}

typeTextByName( textstr, selector ) {
	if ( ieMain.document.getElementsByName(selector).length > 0 ) {
		ieMain.document.getElementsByName(selector)[0].Focus() ; Focus input on the element
		Sleep, 500 ; Wait for focus on element
		ControlSend, Internet Explorer_Server1, %textstr%, ahk_class IEFrame ; Fill out login form	
	}
}

typeTextById( textstr, selector ) {
	if ( ieMain.document.getElementsById(selector).length > 0 ) {
		ieMain.document.getElementsById(selector)[0].Focus() ; Focus input on the element
		Sleep, 500 ; Wait for focus on element
		ControlSend, Internet Explorer_Server1, %textstr%, ahk_class IEFrame ; Fill out login form	
	}
}